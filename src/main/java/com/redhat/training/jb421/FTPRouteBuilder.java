package com.redhat.training.jb421;

import org.apache.camel.builder.RouteBuilder;

public class FTPRouteBuilder extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub 		
		from("ftp://192.168.43.114/other/?username=nanda&password=password1234&delete=true&include=order.*xml")
		.log("New file ${header.CamelFileName} picked up from ${header.CamelFileHost}")
		.process(new ExchangePrinter())
		.to("file:etc/file/out?fileName=${header.CamelFileHost}_${header.CamelFileName}&fileExist=Append");
	}
}
